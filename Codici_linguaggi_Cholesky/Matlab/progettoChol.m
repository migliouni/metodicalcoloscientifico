matrici = ["ex15.mat","shallow_water1.mat","apache2.mat","parabolic_fem.mat","G3_circuit.mat","cfd1.mat","cfd2.mat","StocF-1465.mat","Flan_1565.mat"];
n = size(matrici,2);
for i=1:n
    progettoCholesky(matrici(i));
end

function progettoCholesky(M)
    fprintf ('%s\n', M);
    oraInizio = clock;
    
    tstart = tic;
    load(M);
    tReadStop = toc(tstart);
    fprintf ('%s%d\n',"tempo lettura: " ,tReadStop);
    
    A = Problem.A;
    clear Problem;
    n = size(A, 1);
    xe = ones(1,n)';
    b = A * xe;
    
    tstart2=tic;
    x = A \ b;
    time = toc(tstart2);
    oraFine = clock;
    err = norm(x -xe)/norm(xe);
    fprintf ('%s%d\n',"tempo computare: ", time);
    fprintf ('%s%d\n\n',"errore: ", err);
	
    fid = fopen('prestazione_matlab.txt','a');
    fprintf(fid, '%s\n', M);
    fprintf(fid, '%s%s\n',"ora inizio: ", num2str(oraInizio));   
    fprintf(fid, '%s%s\n',"ora fine: ", num2str(oraFine));    
    fprintf(fid, '%s%s\n',"tempo read: ", num2str(tReadStop));  
    fprintf(fid, '%s%s\n',"tempo calcolo: ", num2str(time)); 
    fprintf(fid, '%s%s\n\n\n',"errore: ", num2str(err));      
    fclose(fid);
    
end
