from cvxopt import cholmod
from cvxpy.interface import matrix_utilities as cv
import cvxpy
import cvxopt
import time
import numpy as np
from scipy.io import loadmat

def computeMatrix(nameMat):
    print(nameMat)
    oraStart = time.strftime("%H:%M:%S")
    treadStart = time.time()
    mat = loadmat(nameMat)
    B = mat['Problem']['A'][0][0]
    treadStop = time.time()

    
    n = B.shape[1]
    A = cv.sparse2cvxopt(B)
    del mat
    del B

    xe = cvxopt.matrix(1, (n,1))
    b = cvxopt.sparse(A * xe)
    
    t0 = time.time()
    x = cholmod.splinsolve(A, b)
    t1 = time.time()

    errore = np.linalg.norm(x-xe)/np.linalg.norm(xe)
    oraFine =time.strftime("%H:%M:%S")
    
    total = t1-t0
    tRead = treadStop - treadStart
    
    f = open('performance_cvxopt.txt', 'a')
    f.write(nameMat + "\n")
    f.write("ora inizio: " +str(oraStart) + "\n")
    f.write("ora fine: " +str(oraFine) + "\n")
    f.write("tempo read: " + str(tRead) + "\n")
    f.write("tempo calcolo: " + str(total) +"\n")
    f.write("errore: " + str(errore) + "\n\n")
    f.close()

def main():
    matrici = ["ex15.mat","shallow_water1.mat","apache2.mat","parabolic_fem.mat","G3_circuit.mat","cfd1.mat","cfd2.mat","StocF-1465.mat","Flan_1565.mat"]
    n = len(matrici)
    for i in range(n):
        computeMatrix(matrici[i])

if __name__ == '__main__':
    main()
