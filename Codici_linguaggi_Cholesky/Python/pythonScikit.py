import time
import numpy as np
from scikits.sparse.cholmod import cholesky
from scipy.io import loadmat

def computeMatrix(nameMat):
    print(nameMat)
    oraInizio = time.strftime("%H:%M:%S")
    treadStart = time.time()

    mat = loadmat(nameMat)
    A = mat['Problem']['A'][0][0]
    treadStop = time.time()
    n = A.shape[0]
    xe = np.ones((n, 2))
    b = A.dot(xe)
    
    t0 = time.time()
    factor = cholesky(A)
    x = factor(b)
    t1 = time.time()
    oraFine = time.strftime("%H:%M:%S")
    
    errore = np.linalg.norm(x-xe)/np.linalg.norm(xe)
    

    total = t1-t0
    tRead = treadStop - treadStart
    
    f = open('performance_scikit.txt', 'a')
    f.write(nameMat + "\n")
    f.write("ora inizio: " +str(oraInizio) + "\n")
    f.write("ora fine: " +str(oraFine) + "\n")
    f.write("tempo read: " + str(tRead) + "\n")
    f.write("tempo calcolo: " + str(total) +"\n")
    f.write("errore: " + str(errore) + "\n\n")
    f.close()


def main():
    matrici =["ex15.mat","shallow_water1.mat","apache2.mat","parabolic_fem.mat","G3_circuit.mat","cfd1.mat","cfd2.mat","StocF-1465.mat","Flan_1565.mat"]
    n = len(matrici)
    for i in range(n):
        computeMatrix(matrici[i])

if __name__ == '__main__':
    main()


