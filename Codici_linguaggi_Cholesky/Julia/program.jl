using MatrixMarket
using LinearAlgebra,SparseArrays
using TickTock
using Dates

function SolveMatrix(MatrixName)
        println("Inizio computazione matrice:")
        println(MatrixName) 
        oraInizio = Dates.format(now(), "HH:MM:SS") 
	
        tick()
	A = MatrixMarket.mmread(MatrixName)
	fineRead = tok()

	n = size(A,2)
	xe = ones(n)
	b = A * xe; 
        
	tick()
	C = cholesky(Hermitian(A))
	x = C\b
        time = tok()

        oraFine = Dates.format(now(), "HH:MM:SS")  
        
	A = nothing
	err = norm(x-xe)/norm(xe)
	
	println(err)
        f = open("performance_Julia.txt", "a")  
        write(f, string(MatrixName," \n"))
	write(f, string("ora inizio: ", oraInizio,"\n"))
	write(f, string("ora fine: ", oraFine,"\n"))
   	write(f, string("tempo read: ", fineRead,"\n"))
	write(f, string("tempo calcolo: ", time,"\n"))
	write(f, string("errore: ", err,"\n","\n"))
        close(f)
        println("-----------")

end 

function main()
    	matrici = ["ex15.mtx","shallow_water1.mtx","apache2.mtx","parabolic_fem.mtx","G3_circuit.mtx","cfd1.mtx","cfd2.mtx","StocF-1465.mtx","Flan_1565.mtx"]
	n = length(matrici)
	for i in collect(1: n) 
        	SolveMatrix(matrici[i])
	end
end

main()