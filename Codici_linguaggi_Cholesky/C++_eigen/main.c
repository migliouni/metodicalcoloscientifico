#include <iostream>
#include <time.h>
#include <chrono>
#include <Eigen/Sparse>
#include <Eigen/SparseCholesky>
#include <Eigen/Eigen>
#include <unsupported/Eigen/SparseExtra>

typedef Eigen::SparseMatrix<double> mat_type;

int main(){
	int size = 9;
	std::string matrix[size] = {"ex15.mtx","shallow_water1.mtx", "apache2.mtx","parabolic_fem.mtx","G3_circuit.mtx","cfd1.mtx","cfd2.mtx","StocF-1465.mtx","Flan_1565.mtx"};
	
	std::ofstream myfile;

	for(int i = 0; i < size; i++){
		myfile.open ("eigen_results.txt", std::ios_base::app);	
		auto start = std::chrono::system_clock::now();
		auto end = std::chrono::system_clock::now();
		std::time_t end_time = std::chrono::system_clock::to_time_t(end);
		myfile << "started computation at " << std::ctime(&end_time);
		
		mat_type tmp;
		std::cout << "Loading matrix " << matrix[i] << std::endl;

		start = std::chrono::system_clock::now();

		Eigen::loadMarket(tmp, "data/" + matrix[i]);
		mat_type A = tmp.selfadjointView<Eigen::Lower>();

		end = std::chrono::system_clock::now();

		std::chrono::duration<double> elapsed_seconds_load = end-start;

		std::cout << "Creating xe..." << std::endl;

		int n = A.rows();
		Eigen::VectorXd xe(n);
		for(int i = 0; i < n; i++){
			xe[i] = 1;
		}

		Eigen::VectorXd b = A * xe;

		Eigen::SimplicialCholesky<mat_type> chol;

		std::cout << "Solving..." << std::endl;
		start = std::chrono::system_clock::now();

		chol.compute(A);

		if(chol.info()!=Eigen::Success) {
	  		std::cout << "decomposition failed" << std::endl;
	  		return 0;
		}

		Eigen::VectorXd x = chol.solve(b);

		end = std::chrono::system_clock::now();

		double error = (x - xe).norm()/(xe.norm());

		std::chrono::duration<double> elapsed_seconds_solve = end-start;
    	end_time = std::chrono::system_clock::to_time_t(end);

  		myfile << matrix[i] << ":\n";
		myfile << "finished computation at " << std::ctime(&end_time);
		myfile << "elapsed time load: " << elapsed_seconds_load.count() << "\n";
		myfile << "elapsed time solve: " << elapsed_seconds_solve.count() << "\n";
		myfile << "error: " << error << "\n\n";
		myfile.close();
	}
}
