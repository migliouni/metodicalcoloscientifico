matrici = {"data_mat/ex15.mat";"data_mat/shallow_water1.mat";"data_mat/apache2.mat";"data_mat/parabolic_fem.mat";"data_mat/G3_circuit.mat";"data_mat/cfd1.mat";"data_mat/cfd2.mat";"data_mat/StocF-1465.mat";"data_mat/Flan_1565.mat"};
n = size(matrici,1);
notNulls = ones(1,n);
errs = ones(1,n);
times_read = ones(1,n);
hours_start = cell(1,n);
times_exec = ones(1,n);
hours_stop = cell(1,n);

for i=1:n
    [notNull, err, time_read, hour_start, time_exec, hour_stop] = solveMat(matrici{i});
    notNulls(i) = notNull;
    errs(i) = err;
    times_read(i) = time_read;
    hours_start{i} = hour_start;
    times_exec(i) = time_exec;
    hours_stop{i} = hour_stop;
end
