function [notNull, err, time_read, hour_start, time_exec, hour_stop] = solveMat(M)
    file = fopen("performance_octave.txt", "a");
    fprintf(file, "%s\n", M);
    printf("\nMatrix: %s\n", M);
    hour_start = strftime ("%H:%M:%S", localtime (time ()));
    fprintf(file, "ora inizio: %s\n", hour_start);
    printf("Reading...\n");
    read_start = tic;
    load(M);
    time_read = toc(read_start);
    printf("Done.\n");
    A = Problem.A;
    notNull = nnz(A);
    n = size(A, 1);
    xe = ones(1,n)';
    b = A * xe;
    printf("Solving system...\n");
    exec_start = tic;
    x = A \ b;
    time_exec = toc(exec_start);
    hour_stop = strftime ("%H:%M:%S", localtime (time ()));
    printf("Done.\n");
    fprintf(file, "ora fine: %s\n", hour_stop);
    err = norm(x -xe)/norm(xe);
    fprintf(file, "tempo read: %d\n", time_read);
    fprintf(file, "tempo calcolo: %d\n", time_exec);
    fprintf(file, "errore: %d\n\n", err);
	fclose(file)
 end