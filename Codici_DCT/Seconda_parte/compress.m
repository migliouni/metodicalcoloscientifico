function compressed = compress(image, f, d)
  [rows columns] = size(image);
  nBlocksRows = floor(rows / f);
  blockVectorR = [f * ones(1, nBlocksRows), rem(rows, f)];
  nBlocksCols = floor(columns / f);
  blockVectorC = [f * ones(1, nBlocksCols), rem(columns, f)];
  blocks = mat2cell(image, blockVectorR, blockVectorC);
  
  if size(blocks(size(blocks,1),size(blocks,2)),1) ~= f
    blocks(size(blocks,1),:) = [];
  endif
  if size(blocks(size(blocks,1),size(blocks,2)),2) ~= f
    blocks(:,size(blocks,2)) = [];
  endif

  freq_blocks = {};
  for i=1:size(blocks,1)
    for j=1:size(blocks,2)
      freq_blocks{i,j} = dct2(blocks{i,j});
      for k=1:f
        for l=1:f
          if k+l > d+1
            freq_blocks{i,j}(k,l) = 0;
          endif
        endfor
      endfor
      blocks{i,j} = idct2(freq_blocks{i,j});
    endfor
  endfor

  compressed = cell2mat(blocks);
  compressed = uint8(compressed); 
end
