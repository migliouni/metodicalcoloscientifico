# import numpy
import numpy as np
 
# import dct
from scipy.fftpack import dct
import math 
import time

def dct_2d(matrix):
	return dct(dct(matrix.T, norm='ortho').T, norm='ortho')


def mydct(f):
	N = len(f)
	a0 = 1 / math.sqrt(N)
	ak = math.sqrt(2 / N)
	c = np.zeros(N)
	
	for k in range(0, N):     
		ck = 0
		if k == 0:
			for i in range(0,N):
				ck = ck + f[i] * math.cos(math.pi* k * (2 * i + 1) / (2 * N))
			ck = a0 * ck
			c[k] = ck
		else:
			for i in range(0, N):
				ck = ck + f[i] * math.cos(math.pi* k * (2 * i + 1) / (2 * N))
			ck = ak * ck
			c[k] = ck
	return c
	
def mydct2(F):
	shape = np.shape(F)
	C = np.zeros(shape)
	for i in range(0, shape[1]):
		C[:, i] = mydct(F[:, i])

  
	for j in range(0, shape[0]):
		C[j, :] = mydct(C[j, :])
	
	return C

	

F =  np.array([[231, 32, 233, 161, 24, 71, 140, 245],
[247, 40, 248, 245, 124, 204, 36, 107],
[234, 202, 245, 167, 9, 217, 239, 173],
[193, 190, 100, 167, 43, 180, 8, 70],
[11, 24, 210, 177, 81, 243, 8, 112],
[97, 195, 203, 47, 125, 114, 165, 181],
[193, 70, 174, 167, 41, 30, 127, 245],
[87, 149, 57, 192, 65, 129, 178, 228]])

C = mydct2(F)

print(C)