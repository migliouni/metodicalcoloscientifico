function [C] = calcDct2(F)
  [m, n] = size(F);
  C = zeros(m, n);
  for i=1: n
   C(:, i) = calcDct(F(:, i)');
  endfor
  
  for j=1: m
   C(j, :) = calcDct(C(j, :));
  endfor
  
endfunction
