function [c] = calcDct(f)
  
  N = size(f)(2);
  a0 = 1 / sqrt(N);
  ak = sqrt(2 / N);
  c = zeros(1, N);
  
  for(k = 1 : N)     #se partissi da 0 sarebbe N-1
    ck = 0;
    if(k == 1)
      for(i = 1: N) #octave conta da 1, altrimenti sarebbero N-1 k e i
        ck = ck + f(i) * cos(pi* ((k-1) * (2 * (i-1) + 1) / (2 * N)));
      endfor
      ck = a0 * ck;
      c(k) = ck;
    else
      for(i = 1: N) 
        ck = ck + f(i) * cos(pi*((k-1) * (2 * (i-1) + 1) / (2 * N)));
      endfor
      ck = ak * ck;
      c(k) = ck;
    endif
  endfor
endfunction
